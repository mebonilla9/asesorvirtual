var registroPqrdControl = {
	serviceUrl:'http://middlebrain.net:80/PqrdMinMinas/',
	enviarRegistroUsuario:function(data,completado){
		return __cnn.ajax({
			'url': registroPqrdControl.serviceUrl+'usuarios/insertar',
			'data': data,
      'completado':completado
		});
	},
	obtenerUsuarioDocumento:function(data,completado){
		return __cnn.ajax({
			'url': registroPqrdControl.serviceUrl+'usuarios/documento',
			'data': data,
      'completado':completado
		});
	},
	loginUsuario:function(data,completado){
		return __cnn.ajax({
			'url': registroPqrdControl.serviceUrl+'usuarios/login',
			'data': data,
      'completado':completado
		});
	}
};
