var that;
var solicitudPqrdVista = {
  init:function(){
    that = this;
    $("#selCategoria").on("change",that.cambioCategoriaTipoSolicitud);
    $("#requestForm").on("submit",that.enviarFormularioPeticion);
		$(".cabecera").ready(that.validarPosicionScrollPeticion);
    $("#goBack").on("click",that.regresarIndex);
    $("#btnSolicitudes").on("click",that.onClickSolicitudes);
    $("#btnNuevaSolicitud").on("click",that.onClickNueva);
    $("#regresarOptionSolicitud").on("click",that.onRegresarClick);
    $("#btnCerrarSesion").on("click",that.cerrarSesionPqrd);
    document.addEventListener("backbutton", that.regresarBackBtnAndroid, false);
    that.obtenerInformacionInicial();
  },
  obtenerInformacionInicial:function(){
		solicitudPqrdControl.obtenerCategorias(that.obtenerCategoriasCompletado);
		solicitudPqrdControl.obtenerTiposSolicitud(that.obtenerTiposSolicitudCompletado);
	},
	obtenerCategoriasCompletado:function(data){
		__dom.ocultarCargador();
		if(data.codigo === 1){
			solicitudPqrdModel.categorias = data.datos;
      $.each(solicitudPqrdModel.categorias,function(i,item){
        $("#selCategoria").append(
          $("<option>").val(item.idCategoria).html(item.catNombre)
        );
      });
		}
	},
	obtenerTiposSolicitudCompletado:function(data){
		__dom.ocultarCargador();
		if(data.codigo === 1){
			solicitudPqrdModel.tipossolicitud = data.datos;
		}
	},
  cambioCategoriaTipoSolicitud:function(event){
    var opcionSeleccionada = $(this).find("option:selected").val();
    console.log("valor seleccionado: "+opcionSeleccionada);
    that.reiniciarTiposSolicitudSelect();
    $.each(solicitudPqrdModel.tipossolicitud,function(i,item){
      console.log("val: "+item.categoria.idCategoria+"/"+opcionSeleccionada);
      if(item.categoria.idCategoria.toString() === opcionSeleccionada){
        console.log("entre: "+i);
        $("#selTipoSolicitud").append(
          $("<option>").val(item.idTsolicitud).html(item.tsNombre)
        );
      }
    });
  },
  reiniciarCategoriasSelect:function(){
    $("#selCategoria-button span").html("Categoria");
    $("#selCategoria").find("option").remove().end().append($("<option>").val(0).html("Categoria")).val("0");
    $.each(solicitudPqrdModel.categorias,function(i,item){
      $("#selCategoria").append(
        $("<option>").val(item.idCategoria).html(item.catNombre)
      );
    });
  },
  reiniciarTiposSolicitudSelect:function(){
    $("#selTipoSolicitud-button span").html("Tipo de solicitud");
    $("#selTipoSolicitud").find("option").remove().end().append($("<option>").val(0).html("Tipo de solicitud")).val("0");
  },
  enviarFormularioPeticion:function(event){
    event.preventDefault();
		var valid =__dom.validarFormulario($("#registerForm"));
		if(valid === true){
      var registroPqrd = {
        soAsunto: $("#txtAsunto").val(),
        soTexto: $("#txtTexto").val(),
        soNumeroRadicado: "",
        idTsolicitud: $("#selTipoSolicitud").val(),
        idUsuario: __app.obtenerUsuarioPqrd().idUsuario,
      };
      solicitudPqrdControl.enviarSolicitudPqrd(registroPqrd,that.enviarSolicitudPqrdCompletado);
    }
  },
  enviarSolicitudPqrdCompletado:function(data){
    __dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
		if(data.codigo == 1){
			var dataAlert={
				mensaje: "La petición ha sido registrada, la respuesta a su solicitud sera enviada a traves de su cuenta de correo electronico",
				callback:null,
				titulo: "Envio de nueva petición",
				nombreBoton: "Aceptar"
			};
			__dom.mostrarDialogo(dataAlert);
      that.reiniciarCategoriasSelect();
      that.reiniciarTiposSolicitudSelect();
      $("#txtAsunto").val("");
      $("#txtTexto").val("");
    }
  },
	validarPosicionScrollPeticion:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentOption").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	},
  validarPosicionScrollNuevaSolicitud:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentRequest").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	},
	regresarIndex:function(){
		window.location= "../index.html";
	},
  regresarBackBtnAndroid:function(){
    if(__app.obtenerUsuarioPqrd()!== null){
			window.location="../index.html";
		}
		else{
			window.location="registroPqrd.html";
		}
  },
  onClickSolicitudes:function(){
    window.location="listaSolicitudes.html";
  },
  onClickNueva:function(){
    $("#divContentOption").css("display","none");
		$("#divContentRequest").css("display","block");
    that.validarPosicionScrollNuevaSolicitud();
  },
  onRegresarClick:function(){
    $("#divContentOption").css("display","block");
		$("#divContentRequest").css("display","none");
  },
  cerrarSesionPqrd:function(){
    __app.eliminarUsuarioPqrd();
    window.location = "registroPqrd.html";
  }
};
