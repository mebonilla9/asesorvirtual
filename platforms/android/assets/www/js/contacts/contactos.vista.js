var contactosVista={
	init:function(){
		try{
			that=this;
			$('#goBack').on('click',that.regresarIndex);
			var data = {
				dir:'1'
			};
			//contactosModel.listaContactos = contactosControl.obtenerlistaDirectorio(data);
			console.log(contactosModel.listaContactos);
			/*var jsonList = JSON.parse(contactosModel.listaContactos);
			console.log("jsonConvertido");
			console.log(jsonList);*/
			that.renderLista();
		}
		catch(err){
			console.log(err.message);
		}
	},
	regresarIndex:function(){
		window.location= '../index.html';
	},
	renderLista:function(){
		$.each(contactosModel.listaContactos,function(i,item){
			var acordItem = $('<div>').addClass('custom-collapsible')
						  			  .attr('data-role','collapsible')
						  			  .append($('<h1>').html(item.nombre));
				$.each(item.oficinas,function(j,oficina){
					acordItem.append(
						$("<div>").addClass('contacto')
								  .append($('<p>').addClass('nombre').html(oficina.nombre))
								  .append($('<p>').html('Horario: '+oficina.horario))
								  .append($('<p>').html('Dirección: '+oficina.direccion))
								  .append($('<p>').addClass('btnDetalles').text('Ver más detalles'))
								  .append($('<div>').addClass('detalles')
								  .append($('<p>').html('Código Postal: '+oficina.codigo_postal))
								  .append($('<p>').html('Ciudad: '+oficina.ciudad))
								  .append($('<p>').html('Departamento: '+oficina.depto))
								  .append($('<p>').html('Teléfono Pbx: '+oficina.telefono_pbx))
								  .append($('<p>').html('Teléfono Nacional: '+oficina.telefono_nacional))
								  .append($('<p>').html('Email: '+oficina.email))
								  .append($('<p>').html('Página Web: '+oficina.pagina_web))
						).on('click', function(){
						var _this = $(this);
							_this.find('div.detalles').toggle('fast');
							_this.find('p.btnDetalles').toggle('fast');
						})
					);
				});
			$('div#acordeon').append(acordItem);
		});
    }
};
