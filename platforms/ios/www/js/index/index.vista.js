var indexVista = {
	ref:null,
	init:function(){
		that=this;
		console.log('Entrada en index');
		$('a#chatRef').on('click',that.irChat);
		$('a#contRef').on('click',that.irContactos);
		$('a#redesRef').on('click',that.irRedes);
		$('a#pqrdRef').on('click',that.irPqrd);
		$('a#normRef').on('click',that.irNormatividad);
	},
	irChat:function(){
		console.log('Entrada en irChat');
		console.log(__app.obtenerUsuarioActual());
		if(__app.obtenerUsuarioActual()!== null){
			console.log('Entrada en irChat 1');
			window.location=indexModel.chatUrl;
		}
		else{
			console.log('Entrada en irChat 2');
			window.location=indexModel.loginUrl;
		}
	},
	irContactos:function(){
		console.log('Entrada en index');
		window.location=indexModel.contactUrl;
	},
	irRedes:function(){
		console.log('Entrada en irRedes');
		that.ref = window.open(encodeURI('http://k-rudy.github.io/phonegap-twitter-timeline?517830971563847680'), '_blank', 'location=no');
		that.ref.addEventListener('loadstop', that.changeBackgroundColor);
        that.ref.addEventListener('exit', that.iabClose);
	},
	irPqrd:function(){
		console.log('Entrada en irPqrd');
		if(__app.obtenerUsuarioPqrd()!==null){
			window.location=indexModel.solicitudPqrdUrl;
		} else {
			window.location=indexModel.registroPqrdUrl;
		}

	},
	irNormatividad:function(){
		console.log('Entrada en irNormatividad');
	},
	changeBackgroundColor:function() {
		console.log('Entre en cambio css');
        that.ref.insertCSS({
            code: "iframe { width: 100% !important; min-width: 100% !important; min-height: 100vh !important; }"
        }, function() {
            console.log("Styles Altered");
        });
    },
    iabClose:function(event) {
    	console.log('Entrada en iabClose');
        that.ref.removeEventListener('loadstop', that.changeBackgroundColor);
        that.ref.removeEventListener('exit', that.iabClose);
    }
};
