var contactosModel = {
	listaContactos:[
		{
			nombre:'Ministerio de Minas y Energía',
			oficinas:[
				{
			        nombre: 'Ministerio de Minas y Energía ', horario: '7:00 a.m. a 4:00 p.m., de Lunes a Viernes', direccion: 'Calle 43 N° 57-31  Centro Administrativo Nacional', codigo_postal: '111321', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2200 300', telefono_nacional: ' 01 8000 910180', email: 'menergia@minminas.gov.co', pagina_web: 'www.minminas.gov.co', facebook: 'ministeriodeminasyenergia', twitter: ' @Minminas_ ', youtube: 'MinisteriodeMinas', flicker: 'minminas', google: '', linkedin: ''
			    }
			]
		},
		{
			nombre:'Agencia Nacional de Minería',
			oficinas:[
				{   
			        nombre: 'Agencia Nacional de Minería ', horario: '8:00 a.m. a 4:30 p.m.  Lunes a Viernes ', direccion: 'Avenida Calle 26 No. 59 - 51 pisos 8,9 y 10', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2201999', telefono_nacional: '01 8000 933833', email: 'contactenos@anm.gov.co', pagina_web: 'www.anm.gov.co', facebook: 'AgenciaNacionaldeMineria', twitter: ' @ANMColombia', youtube: 'anmcolombia', flicker: '', google: ' Agencia Nacional de Minería', linkedin: ' Agencia Nacional de Minería '
			    },
			    {
			        nombre: ' * Grupo de Información y Atención al Minero', horario: '8:00 a.m. a 4:30 p.m.  Lunes a Viernes ', direccion: 'Avenida Calle 26 No 59 - 51 torre 3. Local 107', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2201999 ext. 6000 ', telefono_nacional: '', email: 'contactenos@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			        nombre: 'Punto de atención Regional - Medellín ', horario: '', direccion: 'Calle 32 E No. 76 - 76 Barrio Laureles El Nogal ', codigo_postal: '', ciudad: 'Medellín ', depto: 'Antioquía ', telefono_pbx: '(4) 5205740', telefono_nacional: '', email: 'par.medellin@anm.gov.co, ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			        nombre: 'Punto de atención Regional - Cúcuta ', horario: '', direccion: 'Calle 13A No. 1E-103 Barrio Caobos', codigo_postal: '', ciudad: 'Cúcuta ', depto: 'Norte de Santander', telefono_pbx: '(7) 5720082 - 5726981', telefono_nacional: '', email: 'marisa.fernandez@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			        nombre: 'Punto de atención Regional - Cartagena ', horario: '', direccion: 'Carrera 2 N°12-125, Edificio Minarete Bocagrande, Local 2 ', codigo_postal: '', ciudad: 'Cartagena ', depto: 'Bolivar', telefono_pbx: '(5) 6685871', telefono_nacional: '', email: 'katia.romero@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Calí ', horario: '', direccion: 'Calle 13A No. 100 - 35, oficinas 201 y 202, Edificio Torre Empresarial', codigo_postal: '', ciudad: 'Calí', depto: 'Valle del Cauca ', telefono_pbx: '(2) 5190686', telefono_nacional: '', email: 'nadia.paya@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Quibdó', horario: '', direccion: 'Carrera 6 N° 28-01 Piso 2', codigo_postal: '', ciudad: 'Quibdó', depto: 'Chocó', telefono_pbx: '(4) 6707556', telefono_nacional: '', email: 'cesar.cordoba@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Manizales ', horario: '', direccion: 'Carrera 24 A N° 61-50, Barrio Estrella', codigo_postal: '', ciudad: 'Manizales', depto: 'Caldas', telefono_pbx: '(6) 8973200', telefono_nacional: '', email: 'andrea.arango@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Ibagué', horario: '', direccion: 'Carrera 8 N°19-31, Barrio Interlaken', codigo_postal: '', ciudad: 'Ibagué ', depto: 'Tolima ', telefono_pbx: '(8) 2611678 -2630683 - 2638900', telefono_nacional: '', email: 'aura.vargas@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Bucaramanga ', horario: '', direccion: 'Carrera 20 N° 24-71', codigo_postal: '', ciudad: 'Bucaramanga ', depto: 'Santander ', telefono_pbx: '(7) 6522819', telefono_nacional: '', email: 'eva.mendoza@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Punto de atención Regional - Valledupar ', horario: '', direccion: 'Calle 11 N° 8-79', codigo_postal: '', ciudad: 'Valledupar ', depto: 'Cesar', telefono_pbx: '(5) 5803585 - 5803878', telefono_nacional: '', email: 'rafael.romero@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    },
			    {
			     	nombre: 'Punto de atención Regional - Nobsa', horario: '', direccion: 'Kilómetro 5, vía Sogamoso', codigo_postal: '', ciudad: 'Nobsa', depto: 'Boyacá', telefono_pbx: '(8) 7717620 - 7705466', telefono_nacional: '', email: 'uriel.barrera@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    },
			    {
			     	nombre: 'Punto de atención Regional - Pasto', horario: '', direccion: 'Calle 2 N° 23 A-32 Capusigra, Avenida Panamericana', codigo_postal: '', ciudad: 'Pasto ', depto: 'Nariño ', telefono_pbx: '(2) 7302593', telefono_nacional: '', email: 'javier.narvaez@anm.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero', horario: '', direccion: 'Avenida Calle 26 N° 59-51 Torre 4, Piso 8 ', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2201999 Ext. 5617, 5618, 5619, 5605.  Celular: 3165246215 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Nobsa (Boyacá )', horario: '', direccion: 'Km 5 Vía Sogamoso - Nobsa, Sector Chameza ', codigo_postal: '', ciudad: 'Nobsa', depto: 'Boyacá', telefono_pbx: '(8) 7724130 Celulares: 3103398632 - 3124477118 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
				}, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Ubaté ( Cundinamarca)', horario: '', direccion: 'Km 1.5 vía Ubaté - Bogotá, Vereda Tausavita. ', codigo_postal: '', ciudad: 'Ubaté ', depto: 'Cundinamarca', telefono_pbx: '(1) 8891677 Celular: 3138297517 - 3115079521 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
				}, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Amagá (Antioquia)', horario: '', direccion: 'Calle 30 N° 30-179 Corregimiento Camilo C, sector La Polca. ', codigo_postal: '', ciudad: 'Amagá', depto: 'Antioquía ', telefono_pbx: '(4) 8472292, 8472291 Celular: 3134959265 - 3147218003 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Remedios (Antioquia)', horario: '', direccion: 'Carrera 8 N° 11-85 Barrio la Avanzada, Juan Pablo II ', codigo_postal: '', ciudad: 'Remedios ', depto: 'Antioquía ', telefono_pbx: '(4) 8303502 Celular: 3117162629 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			    	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Cúcuta (Norte de Santander)', horario: '', direccion: 'Avenida Gran Colombia N° 12E - 96 Barrio Colsag, Edificio Terreos, Piso 2, Universidad Francisco de Paula Santander. ', codigo_postal: '', ciudad: 'Cúcuta', depto: 'Norte de Santander', telefono_pbx: '(7) 5720082, 5726981 Celular: 3118864231 - 3103254006 - 3115215261 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Jamundí (Valle del Cauca)', horario: '', direccion: 'Km 3 vía Jamundí - Potreritos.', codigo_postal: '', ciudad: 'Jamundí', depto: 'Valle del Cauca ', telefono_pbx: '(2) 5214001 - 5214002 Celular: 3142387230 - 3125273804 - 3172671735 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Marmato (Caldas)', horario: '', direccion: 'Sector El Atrio Antigua casa parroquial, Piso 2. ', codigo_postal: '', ciudad: 'Marmato', depto: 'Caldas', telefono_pbx: '(6) 8843004 Celular: 3108465313 - 3137505546 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Bucaramanga (Santander)', horario: '', direccion: 'Carrera 20 N° 24 - 71 Barrio Alarcón', codigo_postal: '', ciudad: 'Bucaramanga ', depto: 'Santander ', telefono_pbx: '(7) 6303364 Celular: 3123960042 ', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Atención de Emergencias Mineras Estaciones de Seguridad y Salvamento Minero En Pasto (Nariño)', horario: '', direccion: 'Calle 2 N° 23A-32 Capusigra, Avenida Panamericana', codigo_postal: '', ciudad: 'Pasto ', depto: 'Nariño ', telefono_pbx: '(2) 7302593', telefono_nacional: '', email: '', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			]
		},
		{
			nombre:'Agencia Nacional de Hidrocarburos',
			oficinas:[
				{
			     	nombre: 'Agencia Nacional de Hidrocarburos ', horario: '7:00 a.m. a 4:00 p.m., de Lunes a Viernes', direccion: 'Av. Calle 26 No. 59 - 65 Piso 2, Edificio Cámara Colombiana de Infraestructura', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '5931717 Ext.1529', telefono_nacional: '01 8000 953000', email: 'participacionciudadana@anh.gov.co info@anh.gov.co', pagina_web: 'www.anh.gov.co', facebook: 'ANHColombia', twitter: ' @ANHColombia', youtube: '', flicker: '', google: 'Agencia Nacional de Hidrocarburos ANH', linkedin: ''
			    }
			]
		},
		{
			nombre:'Servicio Geológico Colombiano - SGC',
			oficinas:[
				{
			     	nombre: 'Servicio Geológico Colombiano - SGC  ', horario: '8:00 a.m. a 5:00 p.m., de lunes a viernes', direccion: 'Diagonal 53 N° 34-53', codigo_postal: '4865', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2200200 - 2200100 - 2221811', telefono_nacional: '018000 110842', email: 'cliente@sgc.gov.co', pagina_web: 'www.sgc.gov.co', facebook: 'Servicio Geológico Colombiano', twitter: ' @sgcol', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Manizales', horario: '', direccion: 'Avenida 12 de Octubre N°15-47', codigo_postal: '', ciudad: 'Manizales', depto: 'Caldas', telefono_pbx: '(6) 8843005 - 8843007', telefono_nacional: '', email: 'ovsm@sgc.gov.co  manizales@sgc.gov.co  gpcortes@sgc.gov.co ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Popayán', horario: '', direccion: 'Calle 5B N°2-14, Barrio Loma Cartagena', codigo_postal: '', ciudad: 'Popayán ', depto: 'Cauca', telefono_pbx: '(2) 8242341 - 8242057', telefono_nacional: '', email: 'popayan@sgc.gov.co aagudelo@sgc.gov.co', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Pasto', horario: '', direccion: 'Calle 27 N° 9 este - 25, Barrio La Carolina ', codigo_postal: '', ciudad: 'Pasto ', depto: 'Nariño ', telefono_pbx: '(2) 7320752', telefono_nacional: '', email: 'pasto@sgc.gov.co - dgomez@sgc.gov.co ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Medellín', horario: '', direccion: 'Calle 75 N° 79A-51', codigo_postal: '', ciudad: 'Medellín ', depto: 'Antioquía ', telefono_pbx: '(4) 2644949', telefono_nacional: '', email: 'medellin@sgc.gov.co  mirestrepo@sgc.gov.co ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Cali', horario: '', direccion: 'Carrera 98 N° 16-00', codigo_postal: '', ciudad: 'Calí ', depto: 'Valle del Cauca ', telefono_pbx: '(2) 3395176 - 3393077', telefono_nacional: '', email: 'cali@sgc.gov.co - murreasgc.gov.co ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }, 
			    {
			     	nombre: 'Observatorio Vulcanológico, Sismológico y Grupo de Trabajo Regional Bucaramanga', horario: '', direccion: 'Carrera 20 N° 24-71', codigo_postal: '', ciudad: 'Bucaramanga ', depto: 'Santander ', telefono_pbx: '(7) 6425481', telefono_nacional: '', email: 'bucaramanga@sgc.gov.co - cmendoza@sgc.gov.co ', pagina_web: '', facebook: '', twitter: '', youtube: '', flicker: '', google: '', linkedin: ''
			    }
			]
		},
		{
			nombre:'Instituto de Planificación y Promoción de Soluciones Energéticas para las Zonas No Interconectadas - IPSE',
			oficinas:[
				{
     				nombre: 'Instituto de Planificación y Promoción de Soluciones Energéticas para las Zonas No Interconectadas - IPSE', horario: '8:00 a.m. a 5:00 p.m., de lunes a viernes', direccion: 'Calle 99 No. 9A - 54, Piso 14, Torre 3. Edificio 100 Street.', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 639 7888', telefono_nacional: '01 8000 913468', email: 'ipse@ipse.gov.co', pagina_web: 'www.ipse.gov.co', facebook: 'IPSEMinMinas', twitter: ' @ipse_colombia', youtube: 'Canal - IPSE MinMinas', flicker: '_ipse', google: '', linkedin: ''
    			}
			]
		},
		{
			nombre:'Comisión de Regulación de Energía y Gas - CREG',
			oficinas:[
				{
			     	nombre: 'Comisión de Regulación de Energía y Gas - CREG', horario: '8:00 a.m. a 5:00 p.m., de lunes a viernes', direccion: 'Av. Calle 116 No. 7-15 Edifico Cusezar Int. 2 Oficina 901', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 6032020', telefono_nacional: '01 8000 512734', email: 'creg@creg.gov.co', pagina_web: 'www.creg.gov.co', facebook: '', twitter: '', youtube: 'Creg Colombia', flicker: '', google: '', linkedin: ''
			    }
			]
		},
		{
			nombre:'Unidad de Planeación Minero Energética - UPME',
			oficinas:[
				{
     				nombre: 'Unidad de Planeación Minero Energética - UPME', horario: '7:30 a.m. a 5:00 p.m., de lunes a viernes', direccion: 'Calle 26 N°69D-91 piso 9', codigo_postal: '', ciudad: 'Bogotá D.C.', depto: 'Cundinamarca', telefono_pbx: '(1) 2220601', telefono_nacional: '018000 911729', email: 'info@upme.gov.co upme@upme.gov.co', pagina_web: 'www.upme.gov.co', facebook: 'Upme Oficial', twitter: ' @upmeoficial', youtube: '', flicker: '', google: '', linkedin: ''
    			}
			]
		}
	]
};