var that;
var registroPqrdVista = {
	init:function(){
		that = this;
		$("#loginForm").on("submit",that.enviarFormularioLogin);
		$("#registerForm").on("submit",that.enviarFormularioRegistro);
		$("#btnLoginUser").on("click",that.onLoginClick);
		$("#btnRegisterUser").on("click",that.onRegisterClick);
		$("#regresarOptionLogin").on("click",that.onRegresarClick);
		$("#regresarOptionSignin").on("click",that.onRegresarClick);
	},
	enviarFormularioRegistro:function(event){
		event.preventDefault();
		var valid = __dom.validarFormulario($("#registerForm"));
		if(valid === true){
			var usuarioRegistro = {
				usIdentificacion:$("#txtIdentificacion").val(),
				usPrimerNombre:$("#txtPnombre").val(),
				usSegundoNombre:$("#txtSnombre").val(),
				usPrimerApellido:$("#txtPapellido").val(),
				usSegundoApellido:$("#txtSapellido").val(),
				usCorreo:$("#txtCorreo").val(),
				usTelefono:$("#txtTelefono").val(),
				usCelular:$("#txtCelular").val(),
				usDireccion:$("#txtDireccion").val()
			};
			registroPqrdControl.enviarRegistroUsuario(usuarioRegistro,that.enviarRegistroUsuarioCompletado);
		}
	},
	enviarRegistroUsuarioCompletado:function(data){
		__dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
		if(data.codigo == 1){
			var dataAlert={
				mensaje: "El usuario ha sido registrado",
				callback:null,
				titulo: "Registro de usuario",
				nombreBoton: "Aceptar"
			};
			__dom.mostrarDialogo(dataAlert);
			var docData = {
				us_identificacion: $("#txtIdentificacion").val()
			};
			registroPqrdControl.obtenerUsuarioDocumento(docData,that.obtenerUsuarioDocumentoCompletado);
		}
	},
	obtenerUsuarioDocumentoCompletado:function(data){
		__dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
		if(data.codigo == 1){
			registroPqrdModel.usuarioRegistrado = data.datos;
			__app.almacenarUsuarioPqrd(registroPqrdModel.usuarioRegistrado);
			window.location = "solicitudPqrd.html";
		}
	},
	enviarFormularioLogin:function(event){
		event.preventDefault();
		var valid = __dom.validarFormulario($("#loginForm"));
		if(valid === true){
			var usuarioLogin = {
				usIdentificacion:$("#txtDocumentoSign").val(),
				usCorreo:$("#txtCorreoSign").val()
			};
			registroPqrdControl.loginUsuario(usuarioLogin,that.enviarFormularioLoginCompletado);
		}
	},
	enviarFormularioLoginCompletado:function(data){
		__dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
		if(data.codigo == 1){
			console.log(data.datos);
			if(!$.isEmptyObject(data.datos)){
				registroPqrdModel.usuarioRegistrado = data.datos;
				__app.almacenarUsuarioPqrd(registroPqrdModel.usuarioRegistrado);
				window.location = "solicitudPqrd.html";
			}
			else{
				var dataAlert={
					mensaje: "Usuario no encontrado",
					callback:null,
					titulo: 'Inicio de sesión',
					nombreBoton: 'Aceptar'
				};
				__dom.mostrarDialogo(dataAlert);
			}
		}
	},
	validarPosicionScrollLogin:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentLogin").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	},
	validarPosicionScrollRegistro:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentSignin").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	},
	onLoginClick:function(){
		$("#divContentOption").css("display","none");
		$("#divContentLogin").css("display","block");
		that.validarPosicionScrollLogin();
	},
	onRegisterClick:function(){
		$("#divContentOption").css("display","none");
		$("#divContentSignin").css("display","block");
		that.validarPosicionScrollRegistro();
	},
	onOptionReturn:function(){
		$(".cabecera").removeAttr("style");
		$("#divContentOption").removeAttr("style");
	},
	onRegresarClick:function(){
		$("#divContentOption").css("display","block");
		$("#divContentSignin").css("display","none");
		$("#divContentLogin").css("display","none");
		that.onOptionReturn();
	}
};
