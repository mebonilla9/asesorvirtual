var contactos = [
	{
		nombre: 'Ministerio de Minas y Energía',
		horario: '7:00 a.m. a 4:00 p.m., de Lunes a Viernes',
		direccion: 'Calle 43 No. 57-31 Centro Administrativo Nacional'
		codigo_postal: '111321',
		ciudad: 'Bogotá D.C.',
		depto: 'Cundinamarca',
		telefono_pbx: '(1) 2200 300',
		telefono_nacional: '01 8000 910180',
		email: 'menergia@minminas.gov.co',
		pagina_web: 'www.minminas.gov.co'
	}
	{
		nombre: 'Agencia Nacional de Minería',
		horario: '8:00 a.m. a 4:30 p.m. Lunes a Viernes',
		direccion: 'Avenida Calle 26 No. 59 - 51 pisos 8,9 y 10'
		codigo_postal: '',
		ciudad: 'Bogotá D.C.',
		depto: 'Cundinamarca',
		telefono_pbx: '(1) 2201999',
		telefono_nacional: '01 8000 933833',
		email: 'contactenos@anm.gov.co',
		pagina_web: 'www.anm.gov.co'
	}
	{
		nombre: 'Grupo de Información y Atención al Minero',
		horario: '8:00 a.m. a 4:30 p.m. Lunes a Viernes',
		direccion: 'Avenida Calle 26 No 59 - 51 torre 3. Local 107'
		codigo_postal: '',
		ciudad: 'Bogotá D.C.',
		depto: 'Cundinamarca',
		telefono_pbx: '(1) 2201999',
		telefono_nacional: '01 8000 910180',
		email: 'contactenos@anm.gov.co',
		pagina_web: ''
	}
];