var that;
var detalleSolicitudVista = {
  init:function(){
    that = this;
    $(".cabecera").ready(that.validarPosicionScrollPeticion);
    detalleSolicitudModel.solicitud = __app.obtenerDetalleSolicitud();
    var datoTsol ={
      idTsolicitud:detalleSolicitudModel.solicitud.tsolicitud.idTsolicitud
    };
    detalleSolicitudControl.consultarTipoSolicitud(datoTsol,that.consultarTipoSolicitudCompletado);
  },
  consultarTipoSolicitudCompletado:function(data){
    __dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
    if(data.codigo === 1){
      detalleSolicitudModel.tipoSolicitud = data.datos;
      that.renderizarDetalleSolicitud(detalleSolicitudModel.solicitud,detalleSolicitudModel.tipoSolicitud);
    }
  },
  renderizarDetalleSolicitud:function(solicitud,tipoSolicitud){
    $("#txtAsuntoPeticion").html("Asunto: "+solicitud.soAsunto);
    $("#txtTipoSolicitud").html("Tipo de solicitud: "+tipoSolicitud.tsNombre);
    $("#txtTextoSolicitud").html("Solicitud: "+solicitud.soTexto);
    $("#txtFechaSolicitud").html("Creado el: "+solicitud.soFecha);
    if(solicitud.soResponse!==undefined){
      $("#txtMsgRespuesta").html("Respuesta de la solicitud");
      $("#txtTextoRespuesta").html("Respuesta: "+solicitud.soResponse);
      $("#txtFechaRespuesta").html("Contestado el: "+solicitud.soTimeResponse);
    }
  },
  validarPosicionScrollPeticion:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentOption").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	}
};
