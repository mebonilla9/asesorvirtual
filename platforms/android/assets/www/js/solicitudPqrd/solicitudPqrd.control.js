var solicitudPqrdControl = {
  serviceUrl:'http://middlebrain.net:80/PqrdMinMinas/',
  obtenerCategorias:function(completado){
		return __cnn.ajax({
			'url': solicitudPqrdControl.serviceUrl+'categoriaspqrd/listar',
      'completado':completado
		});
	},
	obtenerTiposSolicitud:function(completado){
		return __cnn.ajax({
			'url': solicitudPqrdControl.serviceUrl+'tipossolicitud/listar',
      'completado':completado
		});
	},
  enviarSolicitudPqrd:function(data,completado){
    return __cnn.ajax({
      'url': solicitudPqrdControl.serviceUrl+'solicitudes/insertar',
      'data': data,
      'completado':completado
    });
  }
};
