var chatVista = {
	myInterval:{},
	init:function(){
		that=this;
		chatModel.currentUsuario = __app.obtenerUsuarioActual();
		console.log(chatModel.currentUsuario);
		if(that.verificarExistenciaHistorial() !== null){
			var html = $.parseHTML(chatModel.currentUsuario.historial);
			$("#divMensajes").append(html);
		}
		$('#btnEnviarChat').on('click',that.enviarPreguntaBoton);
		$('#goBack').on('click',that.regresarIndex);
		$('#goLogout').on('click',that.regresarCerrarSesion);
		$('#txtChat').keypress(that.enviarPreguntaTeclado);
		that.obtenerIdSesion();
		$('div.mensajeMaquina a').on('click',that.responderPreguntaSugerida);
    document.addEventListener("backbutton", that.regresarBackBtnAndroid, false);
		that.validarPosicionScroll();

	},
	enviarPreguntaAsesor:function(pregunta,idPregunta){
		window.clearInterval(that.myInterval);
		console.log("El actual usuario");
		console.log(chatModel.currentUsuario);
		var respuesta = that.enviarPreguntaServidor(pregunta,idPregunta);
		console.log(respuesta);
		that.crearMensajeAsesor(respuesta);
		that.renderPreguntasSugeridas();
	},
	enviarPreguntaBoton:function(e){
		if($('#txtChat').val()!==''){
			var pregunta = $('#txtChat').val();
            console.log("pregunta: "+pregunta);
			console.log('pregunta por boton');
			that.crearMensajeUsuario(pregunta);
			$('#txtChat').val('');
			that.myInterval=setInterval(function(){
				that.enviarPreguntaAsesor(pregunta,'0');
				that.guardarChatArchivo();
			},2000);
		}
	},
	enviarPreguntaTeclado:function(e){
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
            e.preventDefault();
			that.enviarPreguntaBoton();
  		}
	},
	crearMensajeAsesor:function(texto){
		$('#divMensajes').append(
			$('<div>').addClass('mensaje').append(
				$('<div>').addClass('mensajeMaquina').html('<strong>Asesor dice: </strong>'+texto)
			)
		);
		$("body").animate({scrollTop:parseInt($('#divMensajes').css('height'))},600);
	},
	crearMensajeUsuario:function(texto){
		$('#divMensajes').append(
			$('<div>').addClass('mensaje').append(
				$('<div>').addClass('mensajeUsuario').html('<strong>Usuario dice: </strong>'+texto)
			)
		);
		$("body").animate({scrollTop:parseInt($('#divMensajes').css('height'))},600);
	},
	guardarChatArchivo:function(){
		var historial = $('div#divMensajes').html();
		__app.almacenarHistorialUsuario(historial,chatModel.currentUsuario.email);
		$('#txtChat').val('');
	},
	regresarIndex:function(){
		window.location= '../index.html';
	},
	regresarCerrarSesion:function(){
		__app.eliminarUsuarioActual();
		that.regresarIndex();
	},
	verificarExistenciaHistorial:function(){
		console.log('Verificando existencia de historial');
	    try {
	        if(chatModel.currentUsuario.historial !== '' || chatModel.currentUsuario.historial !== undefined || chatModel.currentUsuario.historial !== null){
	        	return chatModel.currentUsuario.historial;
	    	}
	    	else{
	    		return null;
	    	}
	    }
	    catch(err) {
	        console.log(err.message);
	        return null;
	    }
	},
	obtenerIdSesion:function(){
		var data = {
			name: chatModel.currentUsuario.nombre,
			mail: chatModel.currentUsuario.email
		};
		if(window.sessionStorage.getItem('idSession')=== null || window.sessionStorage.getItem('idSession')=== ''){
			var idsession = chatControl.obtenerIdSesion(data);
			console.log('El id de sesion es:');
			console.log('--- '+idsession+' ---');
			window.sessionStorage.setItem('idSession',idsession);
		}
	},
	renderPreguntasSugeridas:function(){
		if($('div.mensajeMaquina:last a').length > 0){
			console.log('tengo etiquetas a');
			var vinculos = $('div.mensajeMaquina:last a');
			vinculos.each(function(i, item){
				item = $(item);
				if(item.attr('onclick')){
					console.log('tengo onclick');
					item.attr('data-myclick',item.attr('onclick').toString());
					item.removeAttr('onclick');
					item.on('click',that.responderPreguntaSugerida);
				}
			});
		}
	},
	responderPreguntaSugerida:function(){
		var link = $(this);
		var linkClick = link.attr('data-myclick');
		console.log("valor del data-myclick");
		console.log(linkClick);
		if(linkClick.indexOf("sendQuestion('")>-1){
			var parts = linkClick.split(',');
		    //con id tiene el valor que necesita
		    var idPregunta = parseInt(parts[1].replace("'", ""));
		    //con q esta la consulta
		    var pregunta = parts[0].split('(')[1].replace(new RegExp("'", 'g'), "");
		    that.crearMensajeUsuario(pregunta);
		   	that.myInterval=setInterval(function(){
				that.enviarPreguntaAsesor(pregunta,idPregunta.toString());
				that.guardarChatArchivo();
			},3000);
		}
		else if(linkClick.indexOf("sendRequest(")>-1){
			var envData = {
				userid : window.sessionStorage.getItem('idSession'),
				botid  : '1',
				qmail  : '1'
			};
			var respuestaMail = chatControl.enviarCorreoServidor(envData);
			console.log(respuestaMail);
		}
	},
	enviarPreguntaServidor:function(pregunta,idPregunta){
		var myData = {
			userid : window.sessionStorage.getItem('idSession'),
			botid  : 'Lucia',
			input  : pregunta,
			name   : chatModel.currentUsuario.nombre,
			mail   : chatModel.currentUsuario.email,
			caid   : idPregunta
    };
		var result = chatControl.obtenerRespuestaAsesor(myData);
		return result;
	},
	validarPosicionScroll:function(){
		if($('#divMensajes').html()!==''){
			$(".cabecera").css({position: 'fixed'});
		}
		$("#divMensajes").css({'margin-top': $('.cabecera').css('height')});
	},
	regresarBackBtnAndroid:function(){
		if(__app.obtenerUsuarioActual()!== null){
			window.location='../index.html';
		}
		else{
			window.location='/login.html';
		}
	}
};
