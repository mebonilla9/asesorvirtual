var loginVista = {
	init:function(){
		that = this;
		console.log('loginView Launched');
		$('#btnIngresar').on('click',that.submitLogin);
    if(__app.obtenerUsuarioActual() !== null){
        __app.eliminarUsuarioActual();
    }
	},
	submitLogin:function(e){
        e.preventDefault();
        console.log('Form Submitted');
        var resultEmail = __app.validarEmail($('#txtUsuario').val());
        console.log('El resultado es: '+resultEmail);
        if(!resultEmail){
        	alert('Debe escribir una direccion de correo valida!');
        }
        else{
        	if($('#nameTxt').val()!== ''){
                usuarioAlmacenar = {
                    email : $('#txtUsuario').val(),
                    nombre : $('#txtPass').val(),
                    historial : {}
                };
                console.log(usuarioAlmacenar);
                var usGet = loginControl.obtenerUsuarioAlmacenado($('#txtUsuario').val());
                console.log('usGet: '+usGet);
                if(usGet <= 0 || usGet === null || usGet === undefined){
                    var resultado = loginControl.guardarUsuario(usuarioAlmacenar);
                    console.log('nuevo usuario guardado? '+resultado);
                    if(resultado){
                        var resp = loginControl.guardarUsuarioActual(usuarioAlmacenar);
                        console.log('nuevo temporal guardado? '+resp);
                        if(resp){
                            window.location = 'chat.html';
                        }
                    }
                }
                else if(usGet.email !== undefined){
                    var respuesta = loginControl.guardarUsuarioActual(usGet);
                    console.log('usuario en localStorage obtenido? '+respuesta);
                    if(respuesta){
                        window.location = 'chat.html';
                    }
                }
                else{
                    console.log('Usuario no encontrado');
                }
            }
            else{
                  alert('Es necesario digitar un nombre de usuario!');
            }
        }
    }
};
