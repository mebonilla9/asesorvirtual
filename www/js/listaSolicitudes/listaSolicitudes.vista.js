var that;
var listaSolicitudesVista = {
  init:function(){
    that = this;
    $(".cabecera").ready(that.validarPosicionScrollPeticion);
    var dataSolicitudes = {
      idUsuario: __app.obtenerUsuarioPqrd().idUsuario
    };
    listaSolicitudesControl.consultarSolicitudesUsuario(dataSolicitudes,that.consultarSolicitudesUsuarioCompletado);
  },
  consultarSolicitudesUsuarioCompletado:function(data){
    __dom.ocultarCargador();
		console.log("Codigo del servidor: "+data.codigo);
    if(data.codigo === 1){
      listaSolicitudesModel.solicitudesUsuario = data.datos;
      $.each(listaSolicitudesModel.solicitudesUsuario,function(i,item){
        $("#lstSolicitudes").append(that.crearItemLista(i,item));
      });
    }
  },
  crearItemLista:function(i,solicitud){
    var item = $("<li>");
    if(i === 0){
      item.addClass("ui-first-child");
    }
    if(i === listaSolicitudesModel.solicitudesUsuario.length){
      item.addClass("ui-last-child");
    }
    item.append(
      $("<a>").addClass("ui-btn ui-btn-icon-right ui-icon-carat-r").append(
        $("<h1>").html(solicitud.soAsunto)
      ).append(
        $("<p>").html(solicitud.soTexto.substring(0,100))
      )
    );
    item.on("click",function(){
      __app.almacenarDetalleSolicitud(solicitud);
      window.location = "detalleSolicitud.html";
    });
    return item;
  },
  validarPosicionScrollPeticion:function(){
		$(".cabecera").css("position","fixed");
		$("#divContentOption").css("margin-top", $(".cabecera").css("height"));
		$(".cabecera").css("z-index","99999");
	}
};
