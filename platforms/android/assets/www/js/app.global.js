var __app = {
    validarEmail:function(mail){
    	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      	return regex.test(mail);
    },
    obtenerUsuarioAlmacenado:function(nombre){
    	console.log('entre a obtenerUsuarioAlmacenado');
        try{
        	var result;
    	    var usuarios = JSON.parse(window.localStorage.getItem('asesorFile'));
    	    if(usuarios!==null || usuarios.length >0 || usuarios !== undefined){
    	        $.each(usuarios, function(i, item){
    	            if(item.email === nombre){
    	                item.posicion = i;
    	                result = item;
    	            }
    	        });
    	    }
    	    return result;
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    },
    almacenarUsuario:function(objeto){
    	console.log('entre a almacenarUsuario');
        try {
            if(window.localStorage.length <= 0 ){
                var usuarios = [{email:'',nombre:'',historial:''}];
                window.localStorage.setItem('asesorFile',JSON.stringify(usuarios));
                var a = JSON.parse(window.localStorage.getItem('asesorFile'));
                console.log(objeto);
                console.log(a);
                a.push(objeto);
                console.log(a);
                window.localStorage.setItem('asesorFile',JSON.stringify(a));
            }
            else{
                var b = JSON.parse(window.localStorage.getItem('asesorFile'));
                console.log(b);
                b.push(objeto);
                console.log(b);
                window.localStorage.setItem('asesorFile',JSON.stringify(b));
            }
            return true;
        }
        catch(err) {
            console.log(err.message);
            return false;
        }
    },
    almacenarUsuarioActual:function(usuario){
    	console.log('entre a almacenarUsuarioActual');
        try {
            window.localStorage.setItem('currentUsuario',JSON.stringify(usuario));
            return true;
        }
        catch(err) {
            console.log(err.message);
            return false;
        }
    },
    obtenerUsuarioActual:function(){
        console.log('entre a obtenerUsuarioActual');
        try {
            return JSON.parse(window.localStorage.getItem('currentUsuario'));
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    },
    eliminarUsuarioActual:function(){
        console.log('entre a eliminarUsuarioActual');
        try {
            window.localStorage.removeItem('currentUsuario');
            return true;
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    },
    almacenarHistorialUsuario:function(hist,mail){
    	console.log('entre a almacenarHistorialUsuario');
    	try{
    	    var usuarios = JSON.parse(window.localStorage.getItem('asesorFile'));
    	    $.each(usuarios, function(i, item){
    	        if(item.email === mail){
    	            item.historial = hist;
                    console.log('El historial es: ');
                    console.log(item.historial);
    	            window.localStorage.setItem('currentUsuario',JSON.stringify(item));
    	        }
    	    });
    	    window.localStorage.setItem('asesorFile',JSON.stringify(usuarios));
        }
        catch(err) {
            console.log(err.message);
        }
    },
    almacenarUsuarioPqrd:function(usuario){
        console.log('entre a almacenarUsuarioPqrd');
        try {
            window.localStorage.setItem('pqrdUsuario',JSON.stringify(usuario));
            return true;
        }
        catch(err) {
            console.log(err.message);
            return false;
        }
    },
    obtenerUsuarioPqrd:function(){
        console.log('entre a obtenerUsuarioPqrd');
        try {
            return JSON.parse(window.localStorage.getItem('pqrdUsuario'));
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    },
    eliminarUsuarioPqrd:function(){
        console.log('entre a eliminarUsuarioPqrd');
        try {
            window.localStorage.removeItem('pqrdUsuario');
            return true;
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    },
    almacenarDetalleSolicitud:function(solicitud){
        console.log('entre a almacenarDetalleSolicitud');
        try {
            window.localStorage.setItem('detalleSolicitud',JSON.stringify(solicitud));
            return true;
        }
        catch(err) {
            console.log(err.message);
            return false;
        }
    },
    obtenerDetalleSolicitud:function(){
        console.log('entre a obtenerDetalleSolicitud');
        try {
            return JSON.parse(window.localStorage.getItem('detalleSolicitud'));
        }
        catch(err) {
            console.log(err.message);
            return null;
        }
    }
};
